# -*- coding: utf-8 -*-

# Scrapy settings for olx project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'olx'

SPIDER_MODULES = ['olx.spiders']
NEWSPIDER_MODULE = 'olx.spiders'

DOWNLOADER_MIDDLEWARES = {
    'scrapy.contrib.downloadermiddleware.httpcache.HttpCacheMiddleware': 50,
    'olx.middlewares.random_proxy.ProxyMiddleware': 100,
    'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
    'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': None,
    'olx.middlewares.random_useragent.RandomUserAgentMiddleware': 400,
}

# Custom exporter to enforce field order
FEED_EXPORTERS = {
    'csv': 'olx.csv_exporter.MyCsvItemExporter'
}

FIELDS_TO_EXPORT = [
    'title',
    'price',
    'time',
    'catloc',
    'catloc_link'
    'adlink'
]

HTTPCACHE_ENABLED = False
HTTPCACHE_EXPIRATION_SECS = 172800  # 2 days

# Retry many times since proxies often fail
RETRY_TIMES = 2

# Retry on most error codes since proxies fail for different reasons
RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 408, 407]

PROXY_LIST = 'olx/data/proxies.txt'
