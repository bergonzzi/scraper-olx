import random
from scrapy import log


class ProxyMiddleware(object):
    def __init__(self, settings):
        proxy_file = settings.get('PROXY_LIST')
        self.proxies = open(proxy_file).read().splitlines()
        log.msg('Initializing with %s proxies' % len(self.proxies))

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)

    def process_request(self, request, spider):
        proxy = random.choice(self.proxies)
        request.meta['proxy'] = proxy
        # log.msg('Using proxy %s for %s, %d proxies left' % (proxy, request.url, len(self.proxies)))

    def process_exception(self, request, exception, spider):
        proxy = request.meta['proxy']

        log.msg('Removing failed proxy <%s>, %d proxies left' % (
            proxy, len(self.proxies) - 1))

        try:
            self.proxies.remove(proxy)
        except ValueError:  # Proxy already removed by another request
            pass
